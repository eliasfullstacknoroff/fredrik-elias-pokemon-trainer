import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon';
import { Trainer } from 'src/app/models/trainer';
import { environment } from 'src/environments/environment';
import { PokemonService } from '../pokemon-service/pokemon.service';
import { TrainerService } from '../trainer.service';

const { API_KEY, trainerAPI } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCollectionService {

  // By ensuring that loading is complete, we avoid working with null/undefined values
  get loading(): boolean {
    return this._loading;
  }

  private _loading: boolean = false;

  // Dependency Injection
  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonService,
    private readonly trainerService: TrainerService
  ) { }

  // Add pokemon to collection
  public addToCollection(url: string): Observable<Trainer> {
    // Error handling
    // If no trainer
    if (!this.trainerService.trainer) {
      throw new Error("addToCollection: There is no trainer");
    }
    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(url);

    // If pokemon doesn't exist
    if (!pokemon) {
      throw new Error("addToCollection: No pokemon with id: " + url);
    }

    // If pokemon is in collection already, remove the pokemon from the collection (un-capture)
    if (this.trainerService.inCollection(url)) {
      this.trainerService.removeFromCollection(url);
    }
    // Otherwise, add it to the collection
    else {
      this.trainerService.addToCollection(pokemon);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': API_KEY
    })

    this._loading = true;

    // Update the trainer's pokemon collection
    return this.http.patch<Trainer>(`${trainerAPI}/${trainer.id}`, {
      pokemon: [...trainer.pokemon]
    }, {
      headers
    }).pipe(
      tap((updatedTrainer: Trainer) => {
        this.trainerService.trainer = updatedTrainer;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }
}
