# Assignment 6: Frontend Development with Angular

Noroff Accelerate Assignment #6, Full-stack .NET development, Frontend

# This project contains the following
## Features
- Angular framework
- Angular Router to navigate between pages
- The username and collected Pokémon is store in the Trainer API which is deployed to Heroku
- Angular Services to manage the state of the application

## Pages
### Landing page/Login page:
- User can enter their Trainer name
- Upon logging in, the Trainer name is stored in the Trainer API and in localstorage
- The user is redirected to the Pokémon Catalogue page on login
- When the user is logged out, any route will redirect to this page (/trainer, /asfhfa, / , => /landing)
- When the user is logged in, they are unable to navigate to the route of this page. If they log out, however, they return to this page automatically

### Trainer page:
- The user can only view this page if they are logged in (a Trainer exists in localstorage)
- This page lists the Trainer name, amount of pokémon caught
- Every pokémon in the Trainer's collection is listed here. Each pokémon has:
  - A name
  - A picture
  - A list of Details, which includes their HP stat, Attack stat and Defense stat
  - A button to release the pokémon (remove from Trainer's collection)

### Pokémon Catalogue Page:
- This is the default page when a user is logged in (trainer exists in localstorage)
- Every pokémon fetched from the API is listed here (limited to the first 151 pokémon in the pokémon universe)
- Likewise to the trainer page, each pokemon is listed with a name, picture, details and a button.
- The button is a toggle, and is displayed as an open pokéball if the Trainer does not have the pokemon in their collection, or a closed pokéball if it exists in their collection

## Component Tree
![Component Tree](/PokemonComponentTree.png?raw=true "Component Tree")

## Contributors

### Fredrik Fauskanger
### Elias Brynestad
</br>

